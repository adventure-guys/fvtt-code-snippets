// Обновить заметки текущей сцены, чтобы не использовать
game.scenes.forEach(scene => {
  scene.notes.forEach(async note => {
    if (note.size !== undefined) {
      await note.document.update({ iconSize: note.size });      
      await note.update({ size: null });
    }
  });
});
ui.notifications.info("Заметки всех сцен были обновлены, чтобы использовать document.iconSize вместо size.");

