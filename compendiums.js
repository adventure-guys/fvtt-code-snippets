async function getCompendiumEntriesNames(compendiumId) {
    // Collect all unique entity names from the specified compendium
    let entities = new Set();  // Use a Set to automatically handle duplicates

    // Function to process the specific compendium and add its entity names to the list
    async function processCompendium(compendium) {
        const content = await compendium.getDocuments();  // Load all documents in the compendium
        content.forEach(entity => entities.add(entity.name));  // Add names to the Set (avoids duplicates)
    }

    // Get the specified compendium
    let compendium = game.packs.get(compendiumId);

    if (compendium) {
        // Process the compendium if it exists
        await processCompendium(compendium);
    } else {
        console.error("Compendium not found: ", compendiumId);
    }

    // Prepare the result array for storing formatted names
    let resultArray = [];

    // Loop through the unique entity names
    entities.forEach(name => {
        let names = name.split(' / ');  // Split the name by " / "

        if (names.length === 2) {
            // If there are both Russian and English names, add both, with English first
            resultArray.push(`${names[1]}\t${names[0]}`);  // English in the first column, Russian in the second
        } else {
            // If there's no separator, add the name in the first column and leave the second empty
            resultArray.push(`${name}\t`);
        }
    });

    // Convert the result array to a string with each entity name on a new line, tab-separated for columns
    return resultArray.join('\n');
}

function getTranslations(a, b, duplicates) {
    let listA = a.trim().split('\n'); // Split into rows for processing
    let listB = b.trim().split('\n'); // Split into rows for processing

    // Function to create a lookup map for List B (English name as key, Russian name as value)
    // It will skip entries without a Russian translation
    function createLookup(list) {
        let map = new Map();
        list.forEach(entry => {
            let [english, russian] = entry.split('\t');

            if (russian?.trim()) {  // Only add entries with a non-empty Russian translation
                const englishKey = english.trim().toLowerCase();
                if (map.has(englishKey)) {
                    if (duplicates[englishKey]) {
                        duplicates[englishKey].push(russian.trim());
                    } else {
                        duplicates[englishKey] = [russian.trim()];
                    }
                }

                map.set(english.trim().toLowerCase(), russian.trim());
            }
        });
        return map;
    }

    // Create a lookup map from List B
    let listBMap = createLookup(listB);

    // Process List A: Fill in missing Russian translations from List B
    let updatedListA = listA.map(entry => {
        let [english, russian] = entry.split('\t');
        let englishTrimmed = english.trim();
        russian = russian?.trim();

        // If Russian translation is missing in List A, try to find it in List B (case-insensitive)
        if (!russian && listBMap.has(englishTrimmed.toLowerCase())) {
            russian = listBMap.get(englishTrimmed.toLowerCase());
        }

        // Return the updated entry (English and Russian names)
        return `${englishTrimmed}\t${russian}`;
    });

    // Convert the updated list to a string with each entry on a new line
    return updatedListA.join('\n');
}

// Function to rename entries in a compendium based on the rule "RussianName / EnglishName"
// with names provided from an Excel-like string input with two columns
async function renameCompendiumEntries(compendiumId, translationString, logNotFoundInGlossary = false) {
    // Get the specified compendium by its ID
    let compendium = game.packs.get(compendiumId);

    if (!compendium) {
        console.error("Compendium not found: ", compendiumId);
        return;
    }

    // Parse the translation string from Excel (tab-separated values)
    let translations = new Map();
    translationString.trim().split('\n').forEach(row => {
        let [english, russian] = row.split('\t').map(name => name.trim());
        if (english && russian) {
            translations.set(english.toLowerCase(), russian);
        }
    });

    // Load all documents in the compendium
    let content = await compendium.getDocuments();
    let notFoundInGlossary = '';

    // Iterate over each entry in the compendium
    for (let entry of content) {
        let names = entry.name.split(' / ');
        // Skip already translated items
        if (names.length > 1) {
            continue;
        }

        let englishName = names.length > 1 ? names[1].trim() : names[0].trim();

        // Find the corresponding Russian name from the translation map
        if (translations.has(englishName.toLowerCase())) {
            let russianName = translations.get(englishName.toLowerCase());

            // Rename the entry following the "RussianName / EnglishName" rule
            let newName = `${russianName} / ${englishName}`;

            // Update the entry with the new name
            await entry.update({name: newName});

            console.log(`Renamed: ${entry.name} to ${newName}`);

            if (logNotFoundInGlossary) {
                notFoundInGlossary += `${englishName}\t${russianName}\n`
            }
        }
    }

    return notFoundInGlossary;
}

async function translateCompendium(id, glossaryStr, translationSrcCompendiumIds) {
    let duplicates = {};
    const compendiumEntries = await getCompendiumEntriesNames(id);
    const translations = getTranslations(compendiumEntries, glossaryStr, duplicates);
    await renameCompendiumEntries(id, translations);

    if (Object.keys(duplicates)?.length) {
        console.warn('POSSIBLE ISSUE WITH GLOSSARY. Terms duplicates found!', duplicates);
    }

    // Using source compendiums to translate terms that were not found in glossary
    duplicates = {};
    let glossaryFromCompendiums = '';
    for (let _id of translationSrcCompendiumIds) {
        const translationEntries = await getCompendiumEntriesNames(_id);
        const translationMap = getTranslations(compendiumEntries, translationEntries, duplicates)
        glossaryFromCompendiums += '\n' + translationMap;
    }

    if (Object.keys(duplicates)?.length) {
        console.warn('Possible danger. Terms duplicates found in COMPENDIUM SOURCES!', duplicates);
    }

    if (glossaryFromCompendiums) {
        const newTerms = await renameCompendiumEntries(id, glossaryFromCompendiums, true);
        
        if (newTerms) {
            console.info('Here are the new terms that needs to be added to glossary!');
            console.info(newTerms);
        }
    }
}

async function revertCompendiumEntriesToOriginalNames(id) {
    let compendium = game.packs.get(id);

    if (!compendium) {
        console.error("Compendium not found: ", id);
        return;
    }

    // Load all documents in the compendium
    let content = await compendium.getDocuments();

    // Iterate over each entry in the compendium
    for (let entry of content) {
        const names = entry.name.split(' / ');
        // Skip not translated items
        if (names.length === 1) {
            continue;
        }

        const englishName = names[1].trim();
        await entry.update({name: englishName});
    }
}