const fs = require('fs');
const path = require('path');
const readline = require('readline');
const util = require('util');
const copyFile = util.promisify(fs.copyFile);

const subfolders = {
  portraits: 'portraits',
  subjects: 'subjects',
  tokens: 'tokens',
}

function getUnifiedFileName(file) {
  return path.parse(file).name.toLowerCase().replace(/[ \-\d]/g, '')
}

const main = async () => {
  try {
    // const rl = readline.createInterface({
    //   input: process.stdin,
    //   output: process.stdout
    // });
    //
    // const askQuestion = (query) => new Promise((resolve) => rl.question(query, resolve));
    //
    // const sourceFolder = await askQuestion('Укажите абсолютный путь до папки "надо сделать": ');
    // const doneFolder = await askQuestion('Укажите абсолютный путь до папки "уже есть": ');
    // const todoFolder = await askQuestion('Укажите абсолютный путь до "осталось": ');
    // const matchedFolder = await askQuestion('Укажите абсолютный путь до папки "подходит": ');
    // TODO change paths
    const sourceFolder = '/Users/nima0718/Desktop/задача/надо сделать';
    const doneFolder = '/Users/nima0718/Desktop/задача/уже есть';
    const todoFolder = '/Users/nima0718/Desktop/задача/осталось';
    const matchedFolder = '/Users/nima0718/Desktop/задача/подходит';

    // rl.close();

    const donePortraits = fs.readdirSync(path.join(doneFolder, subfolders.portraits));
    const donePortraitsWithUnifiedName = donePortraits.map(f => getUnifiedFileName(f));
    const doneTokens = fs.readdirSync(path.join(doneFolder, subfolders.tokens));
    const todoFiles = fs.readdirSync(sourceFolder);

    for (const file of todoFiles) {
      const portraitFileName = getUnifiedFileName(file);
      const portraitIndex = donePortraitsWithUnifiedName.findIndex(f => f === portraitFileName);
      if (portraitIndex > -1) {
        const portrait = donePortraits[portraitIndex];
        const portraitDonePath = path.join(doneFolder, subfolders.portraits, portrait);
        const portraitDestinationPath = path.join(matchedFolder, subfolders.portraits, portrait);
        await copyFile(portraitDonePath, portraitDestinationPath);

        // Some actors can have more than 1 token
        const unifiedFileName = getUnifiedFileName(path.parse(portrait).name);
        const matchingTokens = doneTokens
          .filter(t => getUnifiedFileName(path.parse(t).name) === unifiedFileName);
        // Subjects are always the same as tokens in terms of quantity and name
        const matchingSubjects = matchingTokens
          .map(t => t.replace(subfolders.tokens, subfolders.subjects));

        for (const token of matchingTokens) {
          await copyFile(
            path.join(doneFolder, subfolders.tokens, token),
            path.join(matchedFolder, subfolders.tokens, token)
          );
        }

        for (const subject of matchingSubjects) {
          await copyFile(
            path.join(doneFolder, subfolders.subjects, subject),
            path.join(matchedFolder, subfolders.subjects, subject)
          );
        }
      } else {
        const currentPath = path.join(sourceFolder, file);
        const destinationPath = path.join(todoFolder, subfolders.portraits, file);
        await copyFile(currentPath, destinationPath);
      }
    }

    console.log('Все готово 😎\nНо это не точно, Шеф! 💀');
  } catch (error) {
    console.error('Error:', error);
  }
};

main();
// region copy from laaru
// TODO change paths
// const pathToModules = '/Users/nima0718/Library/Application Support/FoundryVTT/Data';
// const destinationPath = '/Users/nima0718/Desktop/portraitsFromLaaru';
// const copyPortraitImagesFromLaaru = async () => {
//   try {
//     // Ensure destination folder exists
//     if (!fs.existsSync(destinationPath)) {
//       fs.mkdirSync(destinationPath);
//     }
//
//     for (let item of list) {
//       let fileName = item.split('/');
//       fileName = decodeURI(fileName[fileName.length - 1]);
//       await copyFile(path.join(pathToModules, decodeURI(item)), path.join(destinationPath, fileName));
//     }
//   } catch (e) {
//     console.error(e);
//   }
// }
// copyPortraitImagesFromLaaru();
// endregion