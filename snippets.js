// Перегенировать экскизы всех сцен в игровом мире
(async () => {
  for (const scene of game.scenes) {
    try {
        const data = await scene.createThumbnail();
        scene.update({thumb: data.thumb}, {diff: false});
        console.info(game.i18n.format("SCENES.GenerateThumbSuccess", {name: scene.name}));
    } catch(err) {
        console.error(err.message);
    };
  }
  console.info('Эскизы сцен обновлены');
})();

// Обновить путь до изображения с эскизом всех сцен в игровом мире
// Внимание! Не забудьте поменять название мира и модуля на нужные вам
(async () => {
  for (const scene of game.scenes) {
    await scene.update({
      thumb: scene.thumb.replace('worlds/имя-мира/assets/scenes/', 'modules/имя-модуля/assets/scenes/')
    });
  }
  console.info('Сцены обновлены');
})();
// Обновить путь до изображения с эскизом только для тех сцен, у которых путь НЕ ссылкается на модуль
for (let s of game.scenes.contents) {
    if (!s.thumb.startsWith('modules/')) {
        const newPath = s.thumb.replace('worlds/<имя мира>/assets/scenes/', 'modules/<имя-модуля>/assets/scenes/thumb/')
        s.update({ thumb: newPath })
    }
}

// Обновить изображения для всех заметок активной сцены
canvas.scene.notes.contents.forEach(note => {
    note.texture.src = note.texture.src.replace('modules/старый-путь-до-файла/', 'modules/новый путь до файла/');
    note.update({ texture: note.texture });
})

// Обновить путь для всех плейлистов и звуков в них
// Внимание! Не забудьте поменять название пути до файлов на нужные вам
game.playlists.contents.forEach(pl => {
    console.log(pl.name);
    pl.sounds.contents.forEach(s => {
        s.path = s.path.replace('worlds/путь-до-звука-в-мире', 'modules/путь-до-звука-в-модуле')
        s.update({ path: s.path });
    });
})

// Скрыть шкалы показателей у всех выбранных токенов
canvas.tokens.controlled.forEach(i => {
    i.document.update({ displayBars: CONST.TOKEN_DISPLAY_MODES.NONE })
})

// Собрать список путей до портретов всех актеров в папке
const rootFolderId = 'TO BE REPLACED';
let currentFolder = game.actors.folders.get(rootFolderId);
function collectEntries(nestedObject) {
  let entriesList = [];

  function recursiveCollect(obj) {
    // Collect entries if they exist
    if (obj.entries) {
      entriesList = entriesList.concat(obj.entries.map(e => e.img));
    }
    // Recurse into children if they exist
    if (obj.children) {
      for (const key in obj.children) {
        if (obj.children.hasOwnProperty(key)) {
          recursiveCollect(obj.children[key]);
        }
      }
    }
  }

  recursiveCollect(nestedObject);
  return entriesList;
}
collectEntries(currentFolder)

// Поставить всем обычным дверям на всех сценах звук открывания/закрывания
for (let s of game.scenes.contents) {
    const doorsWithoutSound = s.walls.filter(w => w.door === 1 && !w.doorSound);
    console.log(`Updating scene ${s.name}: ${doorsWithoutSound.length} doors`, doorsWithoutSound);
    for (let d of doorsWithoutSound) {
        await d.update({ doorSound: 'woodBasic' });
    }
}

// Обновить шрифт и размер текста у заметок на активной сцене
game.scenes.active.notes.forEach(n => n.update({ fontSize: 120, fontFamily: 'Modesto Condensed' }))